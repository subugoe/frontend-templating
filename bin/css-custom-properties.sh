#!/bin/bash

printf "\n\n---- PROCESS CSS CUSTOM PROPERTIES\n\n"

CSS_SOURCE='./build/css/style.css'
CSS_OUTPUT='./build/css/style.css'

./node_modules/.bin/postcss \
    $CSS_SOURCE \
    --use postcss-custom-properties \
    -o $CSS_OUTPUT