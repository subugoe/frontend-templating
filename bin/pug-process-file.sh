#!/bin/bash

# process a single .pug file from cli argument $1

PUG_SOURCE="$1"
OUTPUT_DIR='./build'

./node_modules/.bin/pug \
    "$PUG_SOURCE" \
    -o $OUTPUT_DIR \
    >> log-file 2>&1