#!/bin/bash

export NODE_ENV=production

rm -rf ./build/ &>/dev/null || true
rm -rf ./demo/ &>/dev/null || true
rm ./log-file &>/dev/null || true
rm ./log-file-last &>/dev/null || true
rm ./log-file-serve &>/dev/null || true
mkdir build &>/dev/null || true

./bin/scss-process.sh
./bin/css-custom-properties.sh
./bin/css-calc.sh
./bin/css-prefix.sh
rm ./build/css/style.css.map &>/dev/null || true
./bin/img-process.sh
./bin/js.sh
./bin/pug-process.sh
./bin/css-minify.sh
./bin/html-minify.sh

rm ./log-file &>/dev/null || true

mv ./build ./demo