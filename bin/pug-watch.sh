#!/bin/bash

WATCH_THIS='src/**/*.{pug,html}'

chokidar \
    "$WATCH_THIS" \
    -c "./bin/pug-process.sh" \
    --verbose