#!/bin/bash

input="log-file"

# cycle through every line in log file
while IFS= read -r line
do

    # check if line begins with "change:" or "add:"
    if [[ $line == "change:"* ]] || [[ $line == "add:"* ]]; then
        echo "❗️ CHANGE DETECTED: $line"

        # check if line references a .pug file
        if [[ $line == *".pug" ]]; then
            # process all .pug files
            if [[ $line == *"/includes/"* ]]; then
                echo "⚙️ PROCESS ALL PUG FILES"
                ./bin/pug-process.sh
            # process the specific .pug file
            else
                echo "⚙️ PROCESS SINGLE PUG FILE"
                ./bin/pug-process.sh "./${line:7}"
            fi
        fi

        # check if line references a .scss file
        if [[ $line == *".scss" ]]; then
            # process all scss
            echo "⚙️ PROCESS SCSS"
            ./bin/scss-process.sh
        fi

        # 
        if [[ $line == *".jpg" ]] || [[ $line == *".png" ]] || [[ $line == *".jpeg" ]]; then
            # process all scss
            echo "⚙️ PROCESS IMAGE"
            ./bin/img-process.sh "./src${line:7}"
        fi

        # check if line references a .js file
        if [[ $line == *".js" ]]; then
            # process all scss
            echo "⚙️ PROCESS IMAGE"
            ./bin/js-process.sh "${line:7}"
        fi

    fi

done < "$input"

# minify rendered html
./bin/html-minify.sh

# reload browser
curl -X POST http://localhost:8001/__lightserver__/trigger

# duplicate log file
cp log-file log-file-last

# empty log file
echo -n "" > log-file