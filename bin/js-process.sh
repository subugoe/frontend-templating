#!/bin/bash

printf "\n\n---- PROCESS JAVASCRIPT\n\n"

# TODO: docu

# TODO: docu
if [[ $1 ]]; then
    # copy single file
    cp -v "./src/js/${1:7}" "./build/js/${1:7}"
# TODO: docu
else
    # copy all files
    cp ./node_modules/jquery/dist/jquery.min.js ./src/js/jquery.min.js
    cp ./node_modules/golden-layout/dist/goldenlayout.min.js ./src/js/goldenlayout.min.js
    cp ./node_modules/prismjs/prism.js ./src/js/prism.js
    cp -r ./src/js ./build/js
fi
