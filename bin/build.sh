#!/bin/bash

export NODE_ENV=production

rm -rf ./build/ &>/dev/null || true
rm -rf ./demo/ &>/dev/null || true
rm ./log-file &>/dev/null || true
rm ./log-file-last &>/dev/null || true
rm ./log-file-serve &>/dev/null || true
mkdir build &>/dev/null || true

./bin/scss-process.sh
./bin/css-custom-properties.sh
./bin/css-calc.sh
./bin/css-prefix.sh
./bin/css-minify.sh

rm ./build/css/style.css.map &>/dev/null || true
rm ./log-file &>/dev/null || true