#!/bin/bash

printf "\n\n---- MINIFY CSS\n\n"

CSS_SOURCE='./build/css/style.css'
CSS_OUTPUT='./build/css/style.css'

./node_modules/.bin/postcss \
    $CSS_SOURCE \
    --use cssnano \
    --no-map \
    -o $CSS_OUTPUT