#!/bin/bash

printf "\n\n---- PROCESS CSS PREFIXING\n\n"

CSS_SOURCE='./build/css/style.css'
CSS_OUTPUT='./build/css/style.css'

./node_modules/.bin/postcss \
    $CSS_SOURCE \
    --use autoprefixer \
    -o $CSS_OUTPUT