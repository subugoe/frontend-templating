#!/bin/bash

chokidar \
    "src/img/**/*.{svg,jpg}" \
    -c "./bin/img-process.sh" \
    --verbose