#!/bin/bash

export NODE_ENV=development

rm -rf ./build/ &>/dev/null || true
rm -rf ./demo/ &>/dev/null || true
rm ./log-file &>/dev/null || true
rm ./log-file-last &>/dev/null || true
rm ./log-file-serve &>/dev/null || true
mkdir build &>/dev/null || true

./bin/pug-process.sh
./bin/img-process.sh
./bin/scss-process.sh
./bin/js-process.sh

touch log-file &>/dev/null || true

./bin/watch-all.sh & ./bin/serve.sh