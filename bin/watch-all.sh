#!/bin/bash

# watch for any change in src folder

chokidar \
    "src/**/*" \
    -c "./bin/get-log-file.sh" \
    -i "log-*" \
    >> log-file 2>&1