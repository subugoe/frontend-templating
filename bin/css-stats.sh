#!/bin/bash

CSS_TARGET='./build/css/style.css'

./node_modules/.bin/cssstats \
    $CSS_TARGET \
    > './report/cssstats/'"$(date +%Y%m%d-%H%M%S)"'.json'