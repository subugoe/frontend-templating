#!/bin/bash

./node_modules/.bin/node-sass \
    './src/scss/bare.scss' \
    --sourceComments true \
    --sourceMap true \
    --sourceMapEmbed true \
    --outputStyle expanded \
    --sourceMapEmbed true \
    --outFile './test/css/bare.css' \
    './test/css/bare.css'

./node_modules/.bin/postcss \
    './test/css/bare.css' \
    --use postcss-custom-properties \
    -o './test/css/bare.css'

./node_modules/.bin/postcss \
    './test/css/bare.css' \
    --use autoprefixer \
    -o './test/css/bare.css'

./node_modules/.bin/postcss \
    './test/css/bare.css' \
    --use autoprefixer \
    -o './test/css/bare.css'

./node_modules/.bin/postcss \
    './test/css/bare.css' \
    --use cssnano \
    --no-map \
    -o './test/css/bare.css'

rm './test/css/bare.css.map'