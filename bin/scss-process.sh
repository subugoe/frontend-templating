#!/bin/bash

# process .scss files

SCSS_SOURCE='./src/scss/style.scss'
SCSS_OUTPUT='./build/css/style.css'

printf "\n\n---- PROCESS SCSS\n\n"

./node_modules/.bin/node-sass \
    $SCSS_SOURCE \
    --sourceComments true \
    --sourceMap true \
    --sourceMapEmbed true \
    --outputStyle expanded \
    --sourceMapEmbed true \
    --outFile $SCSS_OUTPUT \
    $SCSS_OUTPUT