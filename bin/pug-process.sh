#!/bin/bash

printf "\n\n---- PROCESS PUG\n\n"

# process a single .pug file from cli argument $1
if [[ $1 ]]; then
    ./node_modules/.bin/pug \
        "$1" \
        -o ./build \
        >> log-file 2>&1
# process all .pug files
else
    ./node_modules/.bin/pug \
        ./src/index.pug \
        -o ./build

    ./node_modules/.bin/pug \
        ./src/**/*.pug \
        -o ./build
fi
