#!/bin/bash


# npm run build
./node_modules/.bin/lighthouse \
    http://localhost:8001 \
    --output=html \
    --output-path='./report/lighthouse/'"$(date +%Y%m%d-%H%M%S)"'.html' \
    --config-path='./lighthouse-config.js'
