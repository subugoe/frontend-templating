#!/bin/bash

./bin/get-log-file.sh

chokidar \
    "log-file" \
    --initial \
    -c "./bin/get-log-file.sh" \
    --verbose