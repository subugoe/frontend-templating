#!/bin/bash

chokidar \
    "src/scss/**/*.scss" \
    -c "./bin/scss-process.sh" \
    --verbose