#!/bin/bash

cp ./node_modules/jquery/dist/jquery.min.js ./src/js/jquery.min.js
cp ./node_modules/golden-layout/dist/goldenlayout.min.js ./src/js/goldenlayout.min.js
cp ./node_modules/prismjs/prism.js ./src/js/prism.js
cp -r ./src/js ./build/js
