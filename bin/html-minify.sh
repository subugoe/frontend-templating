#!/bin/bash

printf "\n\n---- MINIFY HTML\n\n"

./node_modules/.bin/html-minifier \
    --no-include-auto-generated-tags \
    --file-ext html \
    --remove-comments \
    --collapse-whitespace \
    --input-dir './build/' \
    --output-dir './build/'