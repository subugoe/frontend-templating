#!/bin/bash

./node_modules/.bin/stylelint \
    --fix \
    './src/scss/**/*.scss'