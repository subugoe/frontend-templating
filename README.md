# frontend-templating

## Commands

### npm run dev

build SCSS and PUG files and have them served locally to ```http://localhost:8001/```

CSS will not be minified and a CSS map is provided to make inspecting CSS code easier.

### npm run build

SCSS to CSS from ```src/scss/style.scss``` minified with prefixing, CSS calc / custom properties fallback saved to ```build/css/style.css```

### npm run demo

Used to publish the demo dir to services like netlify to have a demo of what HTML blocks/elements are supported and what CSS is used.

like ```npm run dev``` but without the server feature and minified CSS code (no CSS map file either).

## Misc

### Prefixing

Prefixing values are set in ```.browserlistrc```

Current values:

```txt
last 2 version
> 1%
maintained node versions
not dead
```
