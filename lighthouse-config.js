'use strict';

const config = {
    extends: 'lighthouse:default',
    settings: {
        'onlyCategories': ['performance', 'accessibility', 'seo', 'best-practices'],
        'skipAudits': ['uses-http2']
    }
};
module.exports = config;